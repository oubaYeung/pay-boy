package com.ndood.admin.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.system.RolePo;
import com.ndood.admin.pojo.system.dto.RoleDto;
import com.ndood.admin.pojo.system.dto.TreeDto;
import com.ndood.admin.pojo.system.query.RoleQuery;
import com.ndood.admin.service.system.SystemRoleService;

/**
 * 角色控制器类
 * @author ndood
 */
@Controller
public class SystemRoleController {
	
	@Autowired
	private SystemRoleService systemRoleService;
	
	/**
	 * 显示角色页
	 */
	@GetMapping("/system/role")
	public String toRolePage(){
		return "system/role/role_page";
	}
	
	/**
	 * 显示添加对话框
	 */
	@GetMapping("/system/role/add")
	public String toAddRole(){
		return "system/role/role_add";
	}
	
	/**
	 * 添加角色
	 */
	@PostMapping("/system/role/add")
	@ResponseBody
	public AdminResultVo addRole(@RequestBody RoleDto role) throws Exception{
		systemRoleService.addRole(role);
		return AdminResultVo.ok().setMsg("添加角色成功！");
	}
	
	/**
	 * 删除角色
	 */
	@PostMapping("/system/role/delete")
	@ResponseBody
	public AdminResultVo deleteRole(Integer id){
		Integer[] ids = new Integer[]{id};
		systemRoleService.batchDeleteRole(ids);
		return AdminResultVo.ok().setMsg("删除角色成功！");
	}

	/**
	 * 添加角色
	 */
	@PostMapping("/system/role/batch_delete")
	@ResponseBody
	public AdminResultVo batchDeleteRole(@RequestParam("ids[]") Integer[] ids){
		systemRoleService.batchDeleteRole(ids);
		return AdminResultVo.ok().setMsg("批量删除角色成功！");
	}
	
	/**
	 * 显示修改对话框
	 */
	@GetMapping("/system/role/update")
	public String toUpdateRole(Integer id, Model model){
		RolePo role = systemRoleService.getRole(id);
		model.addAttribute("role", role);
		return "system/role/role_update";
	}
	
	/**
	 * 修改角色
	 * @throws Exception 
	 */
	@PostMapping("/system/role/update")
	@ResponseBody
	public AdminResultVo updateRole(@RequestBody RoleDto role) throws Exception{
		systemRoleService.updateRole(role);
		return AdminResultVo.ok().setMsg("修改角色成功！");
	}
	
	/**
	 * 角色列表
	 * @throws Exception 
	 */
	@PostMapping("/system/role/query")
	@ResponseBody
	public DataTableDto getDectListPage(@RequestBody RoleQuery query) throws Exception{
		DataTableDto page = systemRoleService.pageRoleList(query);
		return page;
	}
	
	/**
	 * 资源树
	 * @param roleId
	 * @return
	 */
	@PostMapping("/system/role/role_permission_tree")
	@ResponseBody
	public AdminResultVo getPermissionTree(Integer roleId){
		List<TreeDto> list = systemRoleService.getPermissionTree(roleId);
		return AdminResultVo.ok().setData(list).setMsg("获取角色权限树成功！");
	}
}
