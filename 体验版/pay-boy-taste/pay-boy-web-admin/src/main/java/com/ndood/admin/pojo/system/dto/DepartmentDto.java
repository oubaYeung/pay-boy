package com.ndood.admin.pojo.system.dto;

import java.util.Date;

import com.ndood.admin.pojo.system.DepartmentPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 部门DTO类
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class DepartmentDto extends DepartmentPo {
	private static final long serialVersionUID = 6650285921381454775L;
	/**
	 * 父部门id
	 */
	private Integer parentId;
	
	public DepartmentDto() {
		super();
	}
	
	public DepartmentDto(Integer id, String name, Integer sort, Integer status, Date createTime, Date updateTime, Integer parentId) {
		super();
		super.setId(id);
		super.setName(name);
		super.setSort(sort);
		super.setStatus(status);
		super.setCreateTime(createTime);
		super.setUpdateTime(updateTime);
		this.parentId = parentId;
	}
	
}
