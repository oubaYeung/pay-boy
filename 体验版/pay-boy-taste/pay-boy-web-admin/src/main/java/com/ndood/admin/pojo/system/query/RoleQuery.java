package com.ndood.admin.pojo.system.query;

import com.ndood.admin.pojo.system.RolePo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 角色查询类
 * @author ndood
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class RoleQuery extends RolePo {
	private static final long serialVersionUID = -5310767646766821702L;
	private Integer offset;
	private Integer limit;
	private String keywords;

	public int getPageNo() {
		return offset / limit;
	}
}
