package com.ndood.admin.core.jpa.entitymanager;

import java.util.Map;

import javax.persistence.Query;

import com.google.common.collect.Maps;

/**
 * em基础类
 */
public class EntityManagerModel {
	
	private EntityManagerModel() {
		super();
	}

	private String sqlPlus = "";
	private String sqlPlus2 = "";
	private String sqlPlus3 = "";
	
	private Map<String,Object> paramsMap = Maps.newTreeMap();
	
	public static EntityManagerModel newModel() {
		return new EntityManagerModel();
	}
	
	public EntityManagerModel plusSQL(String plus) {
		this.sqlPlus += plus;
		return this;
	}
	
	public EntityManagerModel plusSQL2(String plus) {
		this.sqlPlus2 += plus;
		return this;
	}
	
	public String getSQLPlus() {
		return sqlPlus;
	}

	public String getSQLPlus2() {
		return sqlPlus2;
	}
	
	public String getSQLPlus3() {
		return sqlPlus3;
	}
	
	public EntityManagerModel plusSQL3(String plus) {
		this.sqlPlus3 += plus;
		return this;
	}

	public EntityManagerModel plusParam(String key, Object value) {
		this.paramsMap.put(key, value);
		return this;
	}
	
	public EntityManagerModel setQueryParam(Query query) {
		this.paramsMap.forEach((k,v)->{
			query.setParameter(k, v);
		});
		return this;
	}

}
