package com.ndood.admin.pojo.system.dto;

import java.util.Date;

import com.ndood.admin.pojo.system.UserPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 用户DTO类
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class UserDto extends UserPo{
	private static final long serialVersionUID = 7069166002396750862L;

	private Integer provinceId;
	private Integer cityId;
	private Integer districtId;
	private Integer deptId;
	private Integer[] roleIds;
	
	private String departmentName;
	private String provinceName;
	private String cityName;
	private String districtName;
	
	public UserDto() {
	}

	/**
	 * 带星号的邮箱
	 */
	public String getEncryptEmail() {
		String email = getEmail();
		if(email==null) {
			return null;
		}
		email = email.replaceAll("(\\w?)(\\w+)(\\w)(@\\w+\\.[a-z]+(\\.[a-z]+)?)", "$1****$3$4");
		return email;
	}
	
	/**
	 * 带星号的手机
	 */
	public String getEncryptMobile() {
		String mobile = getMobile();
		if(mobile==null) {
			return null;
		}
		mobile = mobile.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
		return mobile;
	}

	public UserDto(Integer id,String nickName,Integer sex, String birthday,
			String headImgUrl,String email, String mobile, Integer status, 
			Date createTime, Date updateTime, String address, Integer provinceId, 
			Integer cityId, Integer districtId, Integer deptId, String departmentName,
			Integer sort) {
		super();
		this.setId(id);
		this.setNickName(nickName);
		this.setSex(sex);
		this.setBirthday(birthday);
		this.setHeadImgUrl(headImgUrl);
		this.setEmail(email);
		this.setMobile(mobile);
		this.setStatus(status);
		this.setCreateTime(createTime);
		this.setUpdateTime(updateTime);
		this.setAddress(address);
		this.setProvinceId(provinceId);
		this.setCityId(cityId);
		this.setDistrictId(districtId);
		this.setDeptId(deptId);
		this.setDepartmentName(departmentName);
		this.setSort(sort);
	}
	
}
