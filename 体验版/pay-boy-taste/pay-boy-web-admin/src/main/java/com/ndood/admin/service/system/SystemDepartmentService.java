package com.ndood.admin.service.system;

import java.util.List;

import com.ndood.admin.pojo.system.dto.DepartmentDto;
import com.ndood.admin.pojo.system.dto.TreeDto;

/**
 * 部门管理业务接口
 * @author ndood
 */
public interface SystemDepartmentService {

	/**
	 * 获取部门tree列表
	 * @param keywords 
	 * @param query
	 * @return
	 * @throws Exception 
	 */
	List<DepartmentDto> getDepartmentList() throws Exception;

	/**
	 * 批量删除部门
	 * @param ids
	 */
	void batchDeleteDepartment(Integer[] ids);

	/**
	 * 添加部门
	 * @param dept
	 * @return 
	 * @throws Exception 
	 */
	DepartmentDto addDepartment(DepartmentDto dept) throws Exception;

	/**
	 * 获取部门信息
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	DepartmentDto getDepartment(Integer id) throws Exception;

	/**
	 * 更新部门信息
	 * @param dept
	 * @return 
	 * @throws Exception 
	 */
	DepartmentDto updateDepartment(DepartmentDto dept) throws Exception;
	
	/**
	 * 获取部门树
	 * @return
	 */
	List<TreeDto> getDepartmentTree();
}
