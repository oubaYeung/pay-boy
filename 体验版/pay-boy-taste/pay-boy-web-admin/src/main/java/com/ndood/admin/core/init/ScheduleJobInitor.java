package com.ndood.admin.core.init;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.ndood.admin.core.quartz.QuartzManager;
import com.ndood.admin.service.system.SystemJobService;
/**
 * 项目初始化
 */
@Component
@Order(value = 1)
public class ScheduleJobInitor implements CommandLineRunner {
	
	@Autowired
	SystemJobService systemJobService;
	
	@Autowired
	QuartzManager quartzManager;
	
	@Override
	public void run(String... arg0) throws Exception {
		try {
			systemJobService.initJobs();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}