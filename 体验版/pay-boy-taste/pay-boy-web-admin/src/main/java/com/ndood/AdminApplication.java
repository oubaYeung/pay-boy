package com.ndood;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Admin启动类
 * https://liuyanzhao.com/
 * @author ndood
 */
@SpringBootApplication
@EnableWebMvc
@EnableSwagger2
@EnableCaching
@EnableTransactionManagement
public class AdminApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(AdminApplication.class, args);
	}
	
}