package com.ndood.admin.service.user;

import com.ndood.admin.core.exception.AdminException;
import com.ndood.admin.pojo.system.dto.UserDto;

public interface AccountInfoService {

	/**
	 * 获取账户信息
	 */
	UserDto getAccountInfoById(String userId) throws Exception;
	/**
	 * 获取基本账户信息
	 */
	UserDto getAccountSimpleInfoById(String userId) throws Exception;
	/**
	 * 更新账户信息
	 */
	void updateAccountInfo(UserDto user) throws Exception;
	/**
	 * 检查手机是否可用 true 可用
	 */
	boolean checkMobile(String mobile);
	/**
	 * 校验邮箱是否可用
	 */
	boolean checkEmail(String email);
	/**
	 * 注册一个账户
	 */
	String registerAccount(UserDto dto) throws Exception;
	/**
	 * 将邮箱状态变成已激活
	 */
	void activeEmail(String username) throws AdminException;
	/**
	 * 校验密码
	 */
	boolean checkPassword(String userId, String pass);
	/**
	 * 修改密码
	 */
	void changePassword(String userId, String password);
	/**
	 * 根据邮箱获取用户信息
	 */
	UserDto getAccountSimpleInfoByEmail(String username) throws Exception;
	/**
	 * 根据手机获取用户信息
	 */
	UserDto getAccountSimpleInfoByMobile(String mobile) throws Exception;

}
