(function($) {
	"use strict";
	// 用于缓存options配置
	$.fn.ndoodDistSelector = function(options, param, param2) {
		// 如果options为方法，则调用字符串对应的方法
		if (typeof options == 'string') {
			return $.fn.ndoodDistSelector.methods[options](this, param, param2);
		}

		// 如果options为json，则根据json进行初始化
		options = $.extend({}, $.fn.ndoodDistSelector.defaults, options || {});
		
		var target = $(this);
		target.prop('temp_options',options);
		
		$.fn.ndoodDistSelector.initDistSelector(target,options);
		return target;
	};
	
	// 初始化combotree
	$.fn.ndoodDistSelector.initDistSelector = function(target, options){
		// 初始化
		var province = $("#" + options.provinceId);
		var city = $("#" + options.cityId);
		var region = $("#" + options.regionId);
		province.html('<option value="">'+options.provinceTips+'</option>');
		city.html('<option value="">'+options.cityTips+'</option>');
		region.html('<option value="">'+options.regionTips+'</option>');

		// 获取init_value，然后移除
		var init_pid = province.attr('init_value');
		province.removeAttr('init_value');
		var init_cid = city.attr('init_value');
		city.removeAttr('init_value');
		var init_rid = region.attr('init_value');
		region.removeAttr('init_value');

		// ajax加载数据并生成树结构
		$.ajax({
			type : 'post',
			url : options.url,
			data : {},
			dataType : "JSON",
			async: false,
			success : function(res, textStatus, jqXHR) {
				if(res.code!='10000'){
					return;
				}
				// 加载省内容
				var list = res.data;
				for(var i=0; i < list.length; i++){
					province.append('<option value="'+list[i][options.id]+'">'+list[i][options.name]+'</option>')
				}
				return false;
			}
		});

		// 初始化省点击事件
		province.change(function(){
			var provinceId = $(this).children('option:selected').val();
			city.html('<option value="">'+options.cityTips+'</option>');
			region.html('<option value="">'+options.regionTips+'</option>');
			if(provinceId==''){
				return;
			}
			$.ajax({
				type : 'post',
				url : options.url,
				data : {provinceId : provinceId},
				dataType : "JSON",
				async: false,
				success : function(res, textStatus, jqXHR) {
					if(res.code!='10000'){
						return;
					}
					// 加载市区内容
					var list = res.data;
					var p = list[0];
					var cs = p.list;
					
					target.prop('dist_citys', cs);
					
					city.html('<option value="">'+options.cityTips+'</option>');
					for(var i=0&&cs!=null; i<cs.length; i++){
						city.append('<option value="'+cs[i][options.id]+'">'+cs[i][options.name]+'</option>')
					}
					return false;
				}
			});
		});
		
		// 初始化市点击事件
		city.change(function(){
			var cityId = $(this).children('option:selected').val();
			region.html('<option value="">'+options.regionTips+'</option>');
			if(cityId==''){
				return;
			}
			
			var cs = target.prop('dist_citys');
			
			for(var i=0&&cs!=null; i<cs.length; i++){
				if(cityId!=cs[i][options.id]){
					continue;
				}
				var rs = cs[i].list;
				for(var j=0&&rs!=null; j<rs.length; j++){
					region.append('<option value="'+rs[j][options.id]+'">'+rs[j][options.name]+'</option>')
				}
				return;
			}
		});
		
		// 如果没有初始值则不触发自动选择
		if(init_pid==undefined||init_cid==undefined||init_rid==undefined){
			return;
		}

		// 自动选择省事件
		province.find('option[value="'+init_pid+'"]').attr("selected",true);
		province.trigger("change");
		
		// 自动选择市事件
		city.find('option[value="'+init_cid+'"]').attr("selected",true);
		city.trigger("change");
		
		// 自动显示区事件
		region.find('option[value="'+init_rid+'"]').attr("selected",true);
	}
	
	// 默认配置
	$.fn.ndoodDistSelector.defaults = {
		provinceId : 'provinceId',
		cityId : 'cityId',
		regionId : 'regionId',
		provinceTips : '请选择省',
		cityTips : '请选择市',
		regionTips : '请选择区',
		id : 'sid',
		name : 'name',
		url : ''
	};
	
	// 自定义方法列表
	$.fn.ndoodDistSelector.methods = {
		reload: function(target){
			var options = target.prop('temp_options');
			$.fn.ndoodDistSelector.initDistSelector(target,options);
		}
	};
})(jQuery);