define([
	'jquery',
	'jquery-validation',
	'jquery-extension',
	'layui',
	'bootstrap-datepicker',
	'bootstrap-datepicker-zh-CN',
	'bootstrap-imguploader',
	'jquery-distselector'
], function() {
	return {
		initPage: function(){
			// 出生日期
			$('#account_info_birthday').datepicker({
				language: 'zh-CN',
				format: 'yyyy-mm-dd',
				autoclose: true
			});
			
			// 初始化地区三级联动组件
			$("#account_info_province_id").ndoodDistSelector({
				provinceId : 'account_info_province_id',
				cityId : 'account_info_city_id',
				regionId : 'account_info_district_id',
				id : 'sid',
				name : 'name',
				url : '/system/region/province'
			});
			
			// 图片上传插件
			$("#account_info_head_image").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				imgName: 'headImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 表单验证
			$("#account_info_form").validate({
				rules: {
				},
				messages:{
				},
			    errorPlacement: function(error,element) {
			    	$(element).parent().updateClass('has-error');
			        $(element).after(error);
			    },
				success:function(element) {
			        $(element).parent().removeClass('has-error');
			        $(element).parent().find('label').remove();
			    },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
			});
		}
	}
});

// 更新账户信息
function update_account_info(){
	// Step1: 表单验证
	var valid = $("#account_info_form").valid();
	if(!valid){
		return;
	}
	var data = $('#account_info_form').serializeJson();
	var arr = [];
	data['roleIds'] = arr;
	
	// Step2: 提交表单
    $.ajax({
    	url:"/user/account/info/update",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:true,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 1});
        		return;
        	}
        	Layout.reloadAjaxContent('/jump_success?url=/user/account/info');
        	return;
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}