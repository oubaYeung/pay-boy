define([
	'jquery',
	'jquery-validation',
	'jquery-extension',
	'layui'
], function() {
	return {
		initPage: function(){
			$("#account_password_form").validate({
				rules: {
					old_password: {
		                required: true,
		                minlength: 5
		            },
		            new_password: {
		                required: true,
		                minlength: 5
		            },
		            reply_password: {
		                required: true,
		                minlength: 5,
		                equalTo:"#account_password_new"
		            },
				},
				messages: {
					old_password: {
		                required: "请输入密码",
		                minlength: "密码长度不能小于 5 个字母"
		            },
		            new_password: {
		                required: "请输入密码",
		                minlength: "密码长度不能小于 5 个字母"
		            },
		            reply_password: {
		                required: "请输入密码",
		                minlength: "密码长度不能小于 5 个字母",
		                equalTo:"两次输入密码不一致"
		            },
		        },

	            // 错误输入框高亮显示
		        highlight: function(element, errorClass, validClass) {
		        	console.log(validClass);
	            	$(element).closest('.form-group').addClass('has-error');
	                $(element).closest('.form-group').find(".form_tips").hide();
	                $(element).closest('.form-group').find(".form_error").show(); 
	            },

	            // 验证成功，移除错误样式
	            unhighlight: function(element, errorClass) {
	            	console.log(errorClass);
	            	$(element).closest('.form-group').removeClass('has-error');
	            	$(element).closest('.form-group').find(".form_tips").show();
	                $(element).closest('.form-group').find(".form_error").hide(); 
	            },

	            // 自定义错误提示位置
	            errorPlacement: function(error, element) {
	            	console.log(error.text());
	            	var errorDom = element.closest('.form-group').find('.form_error');
	            	if(error.text()!=''){
	            		$(errorDom).text(error.text());
	            	}
	            },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
			});
		}
	}
});

// 更新账户信息
function update_account_password(){
	// Step1: 表单验证
	var valid = $("#account_password_form").valid();
	if(!valid){
		return;
	}
	// Step2: 提交表单
    $.ajax({
    	url:"/user/account/password/update",
        type:"post",
        dataType:'json',
        data:$("#account_password_form").serialize(),
        async:true,
        success:function(data){
        	if(data.code!='10000'){
        		layer.alert(data.msg, {icon: 2});
        		return;
        	}
        	layer.alert("修改密码成功，请重新登录",function(){
        		window.location.href = "/signOut";
        	});
        	// Layout.reloadAjaxContent('/jump_success?url=/signOut');
        	return;
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}