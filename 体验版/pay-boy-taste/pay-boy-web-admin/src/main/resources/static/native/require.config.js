/**
 * require.js总配置类
 */
requirejs.config({
	baseUrl: '/',
    waitSeconds: 30,
    charset: 'utf-8',
	urlArgs: "bust=" + (new Date()).getTime(), // 记得部署到生产环境的时候移除掉
	//urlArgs: 'v=1.0.0',
	map: {
        '*': {
            'css': './static/global/plugins/require/css'
        }
    },
	paths: {
		'cookie': [ 
			'https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.0.4/js.cookie.min',
			'./static/global/plugins/js.cookie.min',
		],
		'jquery': [
			'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min',
			'./static/global/plugins/jquery.min',
		],
		'jquery-lazyload': [
			'https://cdnjs.cloudflare.com/ajax/libs/jquery_lazyload/1.9.3/jquery.lazyload',
			'./static/global/plugins/jquery.lazyload.min',
		],
		'jquery-extension': './static/global/plugins/jquery.extension',
		'jquery-validation': [
			'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min',
			'./static/global/plugins/jquery-validation/js/jquery.validate.min',
		],
		'jquery-treetable': './static/global/plugins/jquery-treetable/jquery.treetable',
		'jquery-treetable-extension': './static/global/plugins/jquery-treetable/jquery.treetable.extension',
		'jquery-jstree': [
			'https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.5/jstree.min',
			'./static/global/plugins/jquery-jstree/dist/jstree.min',
		],
		'jquery-combotree': './static/global/plugins/jquery-combotree/jquery-combotree',
		'jquery-distselector': './static/global/plugins/jquery-distselector/jquery-distselector',
		'backstretch': [
			'https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min',
			'./static/global/plugins/backstretch/jquery.backstretch.min',
		],
		'bootstrap': [
			'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min',
			'./static/global/plugins/bootstrap/js/bootstrap.min',
		],
		'bootstrap-datepicker': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min',
			'./static/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker',
		],
		'bootstrap-datepicker-zh-CN': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.zh-CN.min',
			'./static/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.zh-CN',
		],
		'bootstrap-datetimepicker': [
			'https://cdnjs.cloudflare.com/ajax/libs/smalot-bootstrap-datetimepicker/2.4.4/js/bootstrap-datetimepicker.min',
			'./static/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker',
		],
		'bootstrap-datetimepicker-zh-CN': [
			'https://cdnjs.cloudflare.com/ajax/libs/smalot-bootstrap-datetimepicker/2.4.4/js/locales/bootstrap-datetimepicker.zh-CN',
			'./static/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.zh-CN',
		],
		'bootstrap-table': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min',
			'./static/global/plugins/bootstrap-table/js/bootstrap-table',
		],
		'bootstrap-table-mobile': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/extensions/mobile/bootstrap-table-mobile.min',
			'./static/global/plugins/bootstrap-table/js/bootstrap-table-mobile',
		],
		'bootstrap-table-zh-CN': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min',
			'./static/global/plugins/bootstrap-table/js/bootstrap-table-zh-CN',
		],
		'bootstrap-imguploader': './static/global/plugins/bootstrap-imguploader/bootstrap-imguploader',
		'summernote': [
			'https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.min',
			'./static/global/plugins/summernote/summernote.min',
		],
		'summernote-zh-CN': [
			'https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/lang/summernote-zh-CN.min',
			'./static/global/plugins/summernote/lang/summernote-zh-CN.min',
		],
		'bootstrap-content-tabs': './static/layouts/global/plugins/bootstrap-content-tabs/bootstrap-content-tabs',
		'form-repeater': './static/global/scripts/form-repeater.min',
		'layui': './static/global/plugins/layer/layer',
		'app': './static/global/scripts/app.min',
		'layout': './static/layouts/layout/scripts/layout.min',
		'quick-sidebar': './static/layouts/global/scripts/quick-sidebar',
		'quick-nav': './static/layouts/global/scripts/quick-nav',
		
	},
	shim: {
		'cookie':['jquery'],
		'jquery-lazyload': ['jquery'],
		'jquery-validation': {
			deps: ['jquery'],
			exports: '$.validator'
		},
		'jquery-jstree':['jquery'],
		'jquery-treetable': ['jquery'],
		'jquery-treetable-extension':['jquery','jquery-treetable'],
		'jquery-jstree':['jquery'],
		'jquery-combotree':['jquery','jquery-jstree'],
		'jquery-extension': {
			deps: ['jquery','jquery-validation'],
		},
		'bootstrap': ['jquery'],
		'bootstrap-table': {
			deps: ['jquery','bootstrap'],
			exports: '$.fn.bootstrapTable'
		},
		'bootstrap-table-zh-CN': {
			deps: ['jquery','bootstrap-table'],
			exports: '$.fn.bootstrapTable'
		},
		'bootstrap-table-mobile': {
			deps: ['jquery','bootstrap-table'],
			exports: '$.fn.bootstrapTable'
		},
		'bootstrap-datepicker': ['jquery','bootstrap'],
		'bootstrap-datepicker-zh-CN': ['jquery','bootstrap-datepicker'],
		'bootstrap-datetimepicker': ['jquery','bootstrap'],
		'bootstrap-datetimepicker-zh-CN': ['jquery','bootstrap-datetimepicker'],
		'bootstrap-imguploader':['jquery'],
		'summernote':['jquery','bootstrap'],
		'summernote-zh-CN':['jquery','summernote'],
		'bootstrap-content-tabs':['jquery'],
		'form-repeater':['jquery'],
		'layui':['jquery'],
		'backstretch': ['jquery'],
		'app':['jquery','bootstrap'],
		'layout':['jquery','app'],
		'quick-sidebar':['jquery','app'],
		'quick-nav':['jquery','app']
	},
});
