define([
	'jquery',
	'jquery-extension',
	'bootstrap',
	'layui',
	'jquery-validation'
], function($) {
	return {
		init: function(){
			//---------------------------------/手机绑定------------------------------------
			// 1 form表单初始化
	        $('#new_binding_mobile_form').validate({
	            errorElement: 'span', // 默认错误显示位置
	            errorClass: 'help-block', // 默认错误显示样式
	            focusInvalid: true, // 获得焦点时是否验证
	            // 验证规则
	            rules: {
	            	mobile:{
	            		isMobile: true,
	            		required: true
	            	},
	            	imageCode:{
	            		required: true
	            	},
	            	smsCode:{
	            		required: true
	            	}
	            },
	            // 验证提示
	            messages: {
	            	mobile:{
	            		isMobile: '请正确填写您的手机号码！',
	            		required: '手机号不能为空！'
	            	},
	            	imageCode:{
	            		required: '图片验证码不能为空！'
	            	},
	            	smsCode:{
	            		required: '短信验证码不能为空！'
	            	},
	            },
	            // 只要不成功，就显示.alert-dander div
	            invalidHandler: function(event, validator) {
	                $('.alert-danger', $('#new_binding_mobile_form')).show();
	            },
	            // 错误输入框高亮显示
	            highlight: function(element) { 
	                $(element).closest('.form-group').removeClass('valid').addClass('has-error'); 
	            },
	            // 验证成功，移除错误样式
	            success: function(label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },
	            // 自定义错误提示位置
	            errorPlacement: function(error, element) {
	            	var errorDom = $(element).parents('form').find('.alert-danger:eq(0)').find('span:eq(0)');
	            	if(error.text()!=''){
	            		console.log(error.text());
	            		$(errorDom).text(error.text());
	            	}
	            },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
	        });
	        
	        // 2 监听回车事件
	        $('#new_binding_mobile_form input').keypress(function(e) {
	            if (e.which == 13) {
	                if ($('#new_binding_mobile_form').validate().form()) {
	                	$('#new_binding_mobile_submit').click();
	                }
	                return false;
	            }
	        });
	        
	        // 3  验证码
	        $('#mobileCaptcha').click(function() {
	            var $this = $(this);
	            var url = $this.data('src')+'&'+ new Date().getTime();
	            $this.attr('src', url);
	        });
	        
	        // 4 发送短信后倒计时
	        // 获取验证码 多次返回修改手机号后回来重新发送验证码，多次发送的问题
        	$('#mobileSmsCodeBtn').click(function() {
        		var mobile = $('#new_binding_mobile_form').find('input[name="mobile"]').val();
	    		$.ajax({
		        	url: '/code/sms?mobile='+mobile,
		        	type: 'GET',
		        	async: false,
		        	data: {},
		        	success: function(data) {
		                if(data.code=='10000') {
		                	$('#mobileTotalSecond').val(31);
		                	$('#mobileSmsCodeBtn').attr('disabled', true);
		                	$('#mobileSmsCode').val(data.content);
		                	
		                	// 启动定时任务，如果不存在则新建
		                	var intervalId = $('#mobileIntervalId').val();
		                	if(!intervalId){
		                		var interval = setInterval(getOneMoreSMS, 1000);
		                		$('#mobileIntervalId').val(interval);
		                	}
		                } else {
		                	layer.alert('短信发送失败!')
		                }
		        	},
		        });
	        });
	        var getOneMoreSMS = function() {
	        	var second = $('#mobileTotalSecond').val();
	    		if (second == 1) {
	    			var intervalId = $('#mobileIntervalId').val();
                	if(intervalId){
                		$('#mobileIntervalId').val('');
                		clearInterval(intervalId);
                	}
	    			$('#mobileSmsCodeBtn').html('获取短信验证码')
	    			$('#mobileSmsCodeBtn').attr('disabled', false);
	    		} else {
	    			var s = second - 1;
	    			$('#mobileTotalSecond').val(s);
	    			$('#mobileSmsCodeBtn').html('获取短信验证码(' + s + '秒)')
	    		}
	    	}
	        
	        // 5 绑定手机
	        $('#new_binding_mobile_submit').click(function(){
	        	// Step1: form表单校验，远程校验用户名是否被占用。密码字段根据username判断，如果是邮箱注册则需要进行校验
	        	var valid = $('#new_binding_mobile_form').valid();
	        	$('#new_binding_mobile_form').find('input[name="smsCode"]').valid();
	        	$('#new_binding_mobile_form').find('input[name="imageCode"]').valid();
	        	$('#new_binding_mobile_form').find('input[name="mobile"]').valid();
	        	if(!valid){
	        		return;
	        	}
	        	
	        	// Step2: 进行绑定
	        	var data =  $('#new_binding_mobile_form').serializeJson();
	        	$('#new_binding_mobile_submit').button('loading');
	        	$.ajax({
	            	url: '/user/account/binding/new_binding_mobile',
	                type:'post',
	                dataType:'json',
	                contentType:'application/x-www-form-urlencoded',
	                data: data,
	                async:true,
	                success:function(data){
	                	$('#new_binding_mobile_submit').button('reset')
	                	// 处理注册失败
	                	if(data.code!='10000'){
                    		$('.alert-danger', $('#new_binding_mobile_form')).find('span').html(data.msg);
                    		$('.alert-danger', $('#new_binding_mobile_form')).show(); 
                    		$('#mobileCaptcha').click();
                    		return false;
                    	}
	                	
	                	// 注册成功，关闭窗口，并更新父窗口信息
	                	window.opener.refresh_binding_status();
	                	window.opener.layer.alert(data.msg,{icon:1});
	                	var index = window.opener.layer.getFrameIndex(window.name);
	                	window.close();
	                	return false;
	                },
	            });
	        });	
			//---------------------------------手机绑定/------------------------------------
	        
	        //---------------------------------/邮箱绑定------------------------------------
	        // 1 form表单初始化
	        $('#new_binding_email_form').validate({
	            errorElement: 'span', // 默认错误显示位置
	            errorClass: 'help-block', // 默认错误显示样式
	            focusInvalid: true, // 获得焦点时是否验证
	            // 验证规则
	            rules: {
	            	email:{
	            		isEmail: true,
	            		required: true
	            	},
	            	imageCode:{
	            		required: true
	            	},
	            },
	            // 验证提示
	            messages: {
	            	email:{
	            		isEmail: '请正确填写您的邮箱！',
	            		required: '邮箱不能为空！'
	            	},
	            	imageCode:{
	            		required: '图片验证码不能为空！'
	            	},
	            },
	            // 只要不成功，就显示.alert-dander div
	            invalidHandler: function(event, validator) {
	                $('.alert-danger', $('#new_binding_email_form')).show();
	            },
	            // 错误输入框高亮显示
	            highlight: function(element) { 
	                $(element).closest('.form-group').removeClass('valid').addClass('has-error'); 
	            },
	            // 验证成功，移除错误样式
	            success: function(label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },
	            // 自定义错误提示位置
	            errorPlacement: function(error, element) {
	            	var errorDom = $(element).parents('form').find('.alert-danger:eq(0)').find('span:eq(0)');
	            	if(error.text()!=''){
	            		console.log(error.text());
	            		$(errorDom).text(error.text());
	            	}
	            },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
	        });
	        
	        // 2 监听回车事件
	        $('#new_binding_email_form input').keypress(function(e) {
	            if (e.which == 13) {
	                if ($('#new_binding_email_form').validate().form()) {
	                	$('#new_binding_email_submit').click();
	                }
	                return false;
	            }
	        });
	        
	        // 3  验证码
	        $('#emailCaptcha').click(function() {
	            var $this = $(this);
	            var url = $this.data('src')+'&'+ new Date().getTime();
	            $this.attr('src', url);
	        });
	        
	        // 4 绑定邮箱
	        $('#new_binding_email_submit').click(function(){
	        	// Step1: form表单校验，远程校验用户名是否被占用。密码字段根据username判断，如果是邮箱注册则需要进行校验
	        	var valid = $('#new_binding_email_form').valid();
	        	$('#new_binding_email_form').find('input[name="imageCode"]').valid();
	        	$('#new_binding_email_form').find('input[name="email"]').valid();
	        	if(!valid){
	        		return;
	        	}
	        	
	        	// Step2: 进行绑定
	        	var data =  $('#new_binding_email_form').serializeJson();
	        	$('#new_binding_email_submit').button('loading');
	        	$.ajax({
	            	url: '/user/account/binding/new_binding_email',
	                type:'post',
	                dataType:'json',
	                contentType:'application/x-www-form-urlencoded',
	                data: data,
	                async:true,
	                success:function(data){
	                	$('#new_binding_email_submit').button('reset')
	                	// 处理注册失败
	                	if(data.code!='10000'){
                    		$('.alert-danger', $('#new_binding_email_form')).find('span').html(data.msg);
                    		$('.alert-danger', $('#new_binding_email_form')).show(); 
                    		$('#emailCaptcha').click();
                    		return false;
                    	}
	                	
	                	// 跳转到新邮箱激活页面
	                	var email = $('#new_binding_email_form').find('input[name="email"]').val();
	                	goToNewBindingEmailActive(email);
	                	return false;
	                },
	            });
	        });	
	        
	        // 5 去邮箱激活
	        $("#emailToActiveBtn").click(function(){
	        	var hash={ 
    				'qq.com': 'http://mail.qq.com', 
    				'gmail.com': 'http://mail.google.com', 
    				'sina.com': 'http://mail.sina.com.cn', 
    				'163.com': 'http://mail.163.com', 
    				'126.com': 'http://mail.126.com', 
    				'yeah.net': 'http://www.yeah.net/', 
    				'sohu.com': 'http://mail.sohu.com/', 
    				'tom.com': 'http://mail.tom.com/', 
    				'sogou.com': 'http://mail.sogou.com/', 
    				'139.com': 'http://mail.10086.cn/', 
    				'hotmail.com': 'http://www.hotmail.com', 
    				'live.com': 'http://login.live.com/', 
    				'live.cn': 'http://login.live.cn/', 
    				'live.com.cn': 'http://login.live.com.cn', 
    				'189.com': 'http://webmail16.189.cn/webmail/', 
    				'yahoo.com.cn': 'http://mail.cn.yahoo.com/', 
    				'yahoo.cn': 'http://mail.cn.yahoo.com/', 
    				'eyou.com': 'http://www.eyou.com/', 
    				'21cn.com': 'http://mail.21cn.com/', 
    				'188.com': 'http://www.188.com/', 
    				'foxmail.com': 'http://www.foxmail.com' 
	        	};
	        	var email_send_to = $("#emailSendTo").val();
	        	if(email_send_to){
	        		var url = email_send_to.split('@')[1];
	        		window.opener.location.href = hash[url];
	        		window.close();
	        	}
	        });
	        //---------------------------------邮箱绑定/------------------------------------
		}
	}
});

//邮箱注册
function goToNewBindingEmailActive(email){
	$('.regist-content').find('form').hide();
	$('#new_binding_email_form2')[0].reset();
	$("#emailSendTo").val(email); // 设置激活邮箱地址
	
	$('.alert', $('#new_binding_email_form2')).hide()
	$('#email_form_success_tip').find('span').html('绑定成功，请到邮箱中进行激活！<br/>邮件发送至 '+email);
	$('#email_form_success_tip').show();
	$('#new_binding_email_form2').show();
}
