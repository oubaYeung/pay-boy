define([
	'jquery',
	'jquery-validation',
	'jquery-extension',
	'bootstrap',
	'layui',
], function($) {
	return {
		init: function() {
			
			// 设置默认自动登录勾选
			$(".login-form").find('#remember-me').prop("checked",true);
			
	    	// 初始化jquery验证插件
	        $('.login-form').validate({
	            errorElement: 'span', // 默认错误显示位置
	            errorClass: 'help-block', // 默认错误显示样式
	            focusInvalid: true, // 获得焦点时是否验证
	            
	            // 验证规则
	            rules: {
	                username: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                imageCode: {
	                	required: true
	                }
	            },

	            // 验证提示
	            messages: {
	                username: {
	                    required: "用户名或密码为空 !"
	                },
	                password: {
	                    required: "用户名或密码为空 !"
	                },
	                imageCode: {
	                	required: "验证码为空 !"
	                }
	            },

	            // 只要不成功，就显示.alert-dander div
	            invalidHandler: function(event, validator) {
	                $('.alert-danger', $('.login-form')).show();
	            },

	            // 错误输入框高亮显示
	            highlight: function(element) { 
	                $(element).closest('.form-group').removeClass('valid').addClass('has-error'); 
	            },

	            // 验证成功，移除错误样式
	            success: function(label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            // 自定义错误提示位置
	            errorPlacement: function(error, element) {
	            	var errorDom = $(element).parents('form').find('.alert-danger:eq(0)').find('span:eq(0)');
	            	if(error.text()!=''){
	            		console.log(error.text());
	            		$(errorDom).text(error.text());
	            	}
	            },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
	        });
	        
	        // 监听表单登录按钮事件
	        $("#login_form_submit").click(function(){
	        	// 控制表单验证顺序，从用户名密码开始提示。。。
	        	var valid = $('.login-form').valid();
	        	$('.login-form').find("input[name='imageCode']").valid();
	        	$('.login-form').find("input[name='password']").valid();
	        	$('.login-form').find("input[name='username']").valid();

	        	if(!valid){
	        		return false;
	        	}
	        	
	        	// 设置checkbox状态
	        	var isChecked = $('.login-form').find("#remember-me").is(":checked");
	        	var data =  $('.login-form').serializeJson();
	        	data['remember-me'] = 'false';
	        	if(isChecked){
	        		data['remember-me'] = 'true';
	        	}
	        	
	        	$("#login_form_submit").button("loading");
	        	$.ajax({
	            	url:"/authentication/form",
	                type:"post",
	                dataType:'json',
	                contentType:"application/x-www-form-urlencoded",
	                data: data,
	                async:true,
	                success:function(data){
	                	window.location.href = "/";
	                	return false;
	                },
	                error: function (response, ajaxOptions, thrownError) {
	                	$("#login_form_submit").button("reset");
	                	var data = response.responseJSON;
	                    if (response.status == 500) {
	                    	if(data.code!='10000'){
	                    		$('#login_form_error_tip', $('.login-form')).find('span').html(data.content);
	                    		$('#login_form_error_tip', $('.login-form')).show(); 
	                    		$("#captcha").click();
	                    		return false;
	                    	}
	                    }
	                	layer.alert(data.content);
	                	return false;
	                }
	            });
	        });

	        // 监听回车事件
	        $('.login-form input').keypress(function(e) {
	            if (e.which == 13) {
	                if ($('.login-form').validate().form()) {
	                	$("#login_form_submit").click();
	                }
	                return false;
	            }
	        });

	        $('#back-btn').click(function(){
	            $('.login-form').show();
	            $('.mobile-form').hide();
	        });
	        
	        // 验证码
	        $("#captcha").click(function() {
	            var $this = $(this);
	            var url = $this.data("src")+"&"+ new Date().getTime();
	            $this.attr("src", url);
	        });

	        $('.mobile-form input').keypress(function(e) {
	            if (e.which == 13) {
	                if ($('.mobile-form').validate().form()) {
	                    $("#mobile_form_submit").click();
	                }
	                return false;
	            }
	        });

	        $('#mobile-login-btn').click(function(){
	            $('.login-form').hide();
	            $('.mobile-form').show();
	            
	            // 清除表单
	            $('.mobile-form')[0].reset();
	        });
	        
	        // 初始化jquery验证插件
	        $('.mobile-form').validate({
	            errorElement: 'span', // 默认错误显示位置
	            errorClass: 'help-block', // 默认错误显示样式
	            focusInvalid: true, // 获得焦点时是否验证
	            
	            // 验证规则
	            rules: {
	                mobile: {
	                    required: true
	                },
	                smsCode: {
	                    required: true
	                }
	            },

	            // 验证提示
	            messages: {
	            	mobile: {
	                    required: "手机号不能为空 !"
	                },
	                smsCode: {
	                    required: "验证码不能为空 !"
	                }
	            },

	            // 只要不成功，就显示.alert-dander div
	            invalidHandler: function(event, validator) {
	                $('.alert-danger', $('.mobile-form')).show();
	            },

	            // 错误输入框高亮显示
	            highlight: function(element) { 
	                $(element).closest('.form-group').removeClass('valid').addClass('has-error'); 
	            },

	            // 验证成功，移除错误样式
	            success: function(label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            // 自定义错误提示位置
	            errorPlacement: function(error, element) {
	            	var errorDom = $(element).parents('form').find('.alert-danger:eq(0)').find('span:eq(0)');
	            	if(error.text()!=''){
	            		console.log(error.text());
	            		$(errorDom).text(error.text());
	            	}
	            },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
	        });
	        
	        // 监听表单登录按钮事件
	        $("#mobile_form_submit").click(function(){
	        	
	        	var valid = $('.mobile-form').valid();
	        	$('.mobile-form').find("input[name='smsCode']").valid();
	        	$('.mobile-form').find("input[name='mobile']").valid();
	        	
	        	if(!valid){
	        		return false;
	        	}
	        	
	        	$("#mobile_form_submit").button("loading");
	        	$.ajax({
	            	url:"/authentication/mobile",
	                type:"post",
	                dataType:'json',
	                contentType:"application/x-www-form-urlencoded",
	                data: $('.mobile-form').serializeJson(),
	                async:true,
	                success:function(data){
	                	window.location.href = "/";
	                	return false;
	                },
	                error: function (response, ajaxOptions, thrownError) {
	                	$("#mobile_form_submit").button("reset");
	                    if (response.status == 500) {
	                    	var data = response.responseJSON;
	                    	if(data.code!='10000'){
	                    		$('#mobile_form_error_tip', $('.mobile-form')).find('span').html(data.content);
	                    		$('#mobile_form_error_tip', $('.mobile-form')).show(); 
	                    		return false;
	                    	}
	                    }
	                	alert(JSON.stringify(response.responseJSON));
	                	return false;
	                }
	            });
	        });
	        
	        // 获取验证码
	        $('#smsCodeBtn').click(function(e) {
	            if(!$("#mobile").valid()){
	        		$('#mobile_form_error_tip', $('.mobile-form')).show(); 
	            	return;
	            }
	            var mobile = $("#mobile").val()
	            
	    		$.ajax({
		        	url: "/code/sms?mobile="+mobile,
		        	type: "GET",
		        	async: false,
		        	data: {},
		        	success: function(data) {
		                if(data.code=='10000') {
	                		$("#totalSecond").val(31);
	                        $("#smsCodeBtn").attr("disabled", true);
	                        $("#smsCode").val(data.content);
	                        
	                        // 启动定时任务，如果不存在则新建
		                	var intervalId = $("#intervalId").val();
		                	if(!intervalId){
		                		var interval = setInterval(getOneMoreSMS, 1000);
		                		$("#intervalId").val(interval);
		                	}
		                } else {
		                	console.log(JSON.stringify(data));
		                	layer.alert('短信发送失败!')
		                }
		        	},
		        });
	        });
	        
	        var getOneMoreSMS = function() {
	        	var second = $("#totalSecond").val();
	    		if (second == 1) {
	    			// 定时器完成后清除定时器
	    			var intervalId = $("#intervalId").val();
                	if(intervalId){
                		$("#intervalId").val('');
                		clearInterval(intervalId);
                	}
	    			$('#smsCodeBtn').html("获取短信验证码")
	    			$("#smsCodeBtn").attr("disabled", false);
	    		} else {
	    			var s = second - 1;
	    			$("#totalSecond").val(s);
	    			$('#smsCodeBtn').html("获取短信验证码(" + s + "秒)")
	    		}
	    	}
	        
	        // qq登录
	        $("#qq-login").click(function(){
	        	var url = '/qqLogin/qq';
	        	var name = '_blank';
	        	
	            var iWidth=800; //弹出窗口的宽度;
	            var iHeight=600; //弹出窗口的高度;
	            var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
	            var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;        

	            var newWindow = window.open('', name,"height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft+",toolbar=no, menubar=no,  scrollbars=yes,resizable=yes,location=no, status=yes");    
	            if (!newWindow)  
	                return false;  
	            var html = "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";  
	            /*  var values = ['1','2'];
	            if (keys && values)  
	            {  
	               html += "<input type='hidden' name='" + keys + "' value='" + values + "'/>";  
	            }  */
	            html += "</form><script type='text/javascript'>document.getElementById('formid').submit();";  
	            html += "<\/script></body></html>".toString().replace(/^.+?\*|\\(?=\/)|\*.+?$/gi, "");   
	            newWindow.document.write(html);
	        });
	        
	        // 微信登录
	        $("#wechat-login").click(function(){
	        	var url = '/qqLogin/weixin';
	        	var name = '_blank';
	        	
	            var iWidth=800; //弹出窗口的宽度;
	            var iHeight=600; //弹出窗口的高度;
	            var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
	            var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;        

	            var newWindow = window.open('', name,"height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft+",toolbar=no, menubar=no,  scrollbars=yes,resizable=yes,location=no, status=yes");    
	            if (!newWindow)  
	                return false;  
	            var html = "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";  
	            html += "</form><script type='text/javascript'>document.getElementById('formid').submit();";  
	            html += "<\/script></body></html>".toString().replace(/^.+?\*|\\(?=\/)|\*.+?$/gi, "");   
	            newWindow.document.write(html);
	        });
	    }
	}
});