package com.ndood.core.social.qq.connect;

import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;

import com.ndood.core.social.qq.api.QQ;
import com.ndood.core.social.qq.api.QQImpl;

/**
 * 新建QQServiceProvider用来生成ConnectionFactory
 */
public class QQServiceProvider extends AbstractOAuth2ServiceProvider<QQ>{

	private String appId;
	
	private static final String URL_AUTHORIZE = "https://graph.qq.com/oauth2.0/authorize";
	
	private static final String URL_ACCESS_TOKEN = "https://graph.qq.com/oauth2.0/token";
	
	/**
	 * 构造方法
	 */
	public QQServiceProvider(String appId, String appSecret) {
		// 配置自定义的qq Oauth2Template
		super(new QQOAuth2Template(appId, appSecret, URL_AUTHORIZE, URL_ACCESS_TOKEN));
		this.appId = appId;
	}
	
	/**
	 * 获取qq Oauth2API
	 */
	@Override
	public QQ getApi(String accessToken) {
		return new QQImpl(accessToken, appId);
	}
}
