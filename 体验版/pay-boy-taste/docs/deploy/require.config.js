/**
 * require.js总配置类
 */
requirejs.config({
	baseUrl: '/',
    waitSeconds: 30,
    charset: 'utf-8',
	// urlArgs: "bust=" + (new Date()).getTime(), // 记得部署到生产环境的时候移除掉
	urlArgs: 'v=1.0.2',
	map: {
        '*': {
            'css': './global/plugins/require/css'
        }
    },
	paths: {
		'cookie': './global/plugins/js.cookie.min',
		'jquery': './global/plugins/jquery.min',
		'jquery-lazyload': '/global/plugins/jquery.lazyload.min',
		'jquery-extension': './global/plugins/jquery.extension',
		'jquery-validation': './global/plugins/jquery-validation/js/jquery.validate.min',
		'jquery-treetable': './global/plugins/jquery-treetable/jquery.treetable',
		'jquery-treetable-extension': './global/plugins/jquery-treetable/jquery.treetable.extension',
		'jquery-jstree': './global/plugins/jquery-jstree/dist/jstree.min',
		'jquery-combotree': './global/plugins/jquery-combotree/jquery-combotree',
		'jquery-distselector': './global/plugins/jquery-distselector/jquery-distselector',
		'backstretch': './global/plugins/backstretch/jquery.backstretch.min',
		'bootstrap': './global/plugins/bootstrap/js/bootstrap.min',
		'bootstrap-datepicker': './global/plugins/bootstrap-datepicker/js/bootstrap-datepicker',
		'bootstrap-datepicker-zh-CN': './global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.zh-CN',
		'bootstrap-datetimepicker': './global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker',
		'bootstrap-datetimepicker-zh-CN': './global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.zh-CN',
		'bootstrap-table': './global/plugins/bootstrap-table/js/bootstrap-table',
		'bootstrap-table-mobile': './global/plugins/bootstrap-table/js/bootstrap-table-mobile',
		'bootstrap-table-zh-CN': './global/plugins/bootstrap-table/js/bootstrap-table-zh-CN',
		'bootstrap-imguploader':'./global/plugins/bootstrap-imguploader/bootstrap-imguploader',
		'summernote': './global/plugins/summernote/summernote.min',
		'summernote-zh-CN': './global/plugins/summernote/lang/summernote-zh-CN.min',
		'bootstrap-content-tabs':'./layouts/global/plugins/bootstrap-content-tabs/bootstrap-content-tabs',
		'form-repeater': './global/scripts/form-repeater.min',
		'layui': './global/plugins/layer/layer',
		'app':'./global/scripts/app.min',
		'layout':'./layouts/layout/scripts/layout.min',
		'quick-sidebar':'./layouts/global/scripts/quick-sidebar',
		'quick-nav':'./layouts/global/scripts/quick-nav',
	},
	shim: {
		'cookie':['jquery'],
		'jquery-lazyload': ['jquery'],
		'jquery-validation': {
			deps: ['jquery'],
			exports: '$.validator'
		},
		'jquery-jstree':['jquery'],
		'jquery-treetable': ['jquery'],
		'jquery-treetable-extension':['jquery','jquery-treetable'],
		'jquery-jstree':['jquery'],
		'jquery-combotree':['jquery','jquery-jstree'],
		'jquery-extension': {
			deps: ['jquery','jquery-validation'],
		},
		'bootstrap': ['jquery'],
		'bootstrap-table': {
			deps: ['jquery','bootstrap'],
			exports: '$.fn.bootstrapTable'
		},
		'bootstrap-table-zh-CN': {
			deps: ['jquery','bootstrap-table'],
			exports: '$.fn.bootstrapTable'
		},
		'bootstrap-table-mobile': {
			deps: ['jquery','bootstrap-table'],
			exports: '$.fn.bootstrapTable'
		},
		'bootstrap-datepicker': ['jquery','bootstrap'],
		'bootstrap-datepicker-zh-CN': ['jquery','bootstrap-datepicker'],
		'bootstrap-datetimepicker': ['jquery','bootstrap'],
		'bootstrap-datetimepicker-zh-CN': ['jquery','bootstrap-datetimepicker'],
		'bootstrap-imguploader':['jquery'],
		'summernote':['jquery','bootstrap'],
		'summernote-zh-CN':['jquery','summernote'],
		'bootstrap-content-tabs':['jquery'],
		'form-repeater':['jquery'],
		'layui':['jquery'],
		'backstretch': ['jquery'],
		'app':['jquery','bootstrap'],
		'layout':['jquery','app'],
		'quick-sidebar':['jquery','app'],
		'quick-nav':['jquery','app']
	},
});
